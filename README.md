# Ambiente Ansible Docker

Esse repositório contêm um ambiente de aprendizagem e experimentação com o Ansible.
O ambiente definido no Docker Compose é composto de 2 serviços:
- `ansible-controller`: é onde o Ansible está instalado, e é onde devemos conectar para executar os comandos Ansible que vão controlar as outras máquinas. A imagem do container é baseada no Alpine Linux com o Ansible instalado, que está definida no arquivo [`Dockerfile.controller`](docker/Dockerfile.controller).
- `ansible-host`: são as máquinas que serão controladas pelo controller e terão suas configurações alteradas via Ansible. A imagem destes containers é uma imagem simples do Ubuntu com um servidor SSH instalado para permitir a conexão remota, que está definida no arquivo [`Dockerfile.host`](docker/Dockerfile.host).

Por padrão, o ambiente sobe 2 máquinas `ansible-host` para serem controladas pelo `ansible-controller`. Caso deseje simular um ambiente com um número diferente de máquinas, siga os seguintes passos:
- Altere o número de réplicas do serviço `ansible-host` no arquivo [`compose.yml`](docker/compose.yml).
- Adicione uma linha para cada réplica no arquivo [`inventory.ini`](inventory.ini), seguindo o padrão já existente.

Como o Docker Compose usa o nome da pasta onde está o `compose.yml` para definir o nome dos containers, o arquivo `compose.yml` está na pasta `docker` para padronizar o nome dos containers gerados, a fim de que possamos ter um arquivo `inventory.ini` estático. Se você mudar o nome da pasta `docker` ou mover o `compose.yml` para outra pasta, lembre de alterar o nome das máquinas no arquivo `inventory.ini`.


## Preparação
Para preparar e subir o ambiente siga os seguintes passos:
1. Execute `ssh-keygen -t rsa -f id_ansible -N ""` na raiz do repositório para gerar um par de chaves SSH sem senha. Esse passo só precisa ser executado uma única vez, pois as chaves serão embutidas nas imagens dos containers.
2. Entre na pasta `docker` com o comando `cd docker` e execute `docker compose up -d` para compilar as imagens Docker e criar os containers de forma _detached_.
3. Quando terminar de usar o ambiente, execute `docker compose down` (também dentro da pasta `docker`) para pará-lo e destruir as máquinas.

## Configurando os hosts
1. Com o ambiente rodando, dentro da pasta `docker` execute `docker compose exec -it --index 1 ansible-host service ssh start` para iniciar o servidor SSH no host 1.
2. Repita o comando anterior para cada réplica que você tiver no serviço `hosts`, trocando o valor do parâmetro `--index`. Por padrão, são 2 réplicas, portanto basta executar mais uma vez trocando `--index 1` por `--index 2`.

### Testando o Ansible no Controller
1. Execute `docker compose exec -it ansible-controller /bin/sh` dentro da pasta `docker` para abrir um terminal no controller.
2. Execute `ansible hosts -m ping` para testar a comunicação entre o controller e os hosts. Se você não iniciou o servidor SSH nos hosts (veja seção anterior) esse teste falhará com a mensagem "_UNREACHABLE. Failed to connect to the host via ssh: ssh: connect to host docker-ansible-host-N port 22: Connection refused_".

### Executando o playbook de exemplo
1. Execute `docker compose ps` dentro da pasta `docker` e verifique quais são as portas que foram mapeadas para cada um dos hosts. O compose está configurado para usar portas entre 8000 e 8090.
2. Tente acessar `http://localhost:8000` (trocando `8000` para a porta que foi mapeada por algum de seus hosts) no seu navegador e verifique que a página apresenta um erro (não há nada rodando ainda).
3. Execute `docker compose exec -it ansible-controller /bin/sh` dentro da pasta `docker` para abrir um terminal no controller.
4. Execute `ansible-playbook playbooks/instala-webservers.yml` para rodar o playbook de exemplo.
5. No seu navegador, acesse novamente o endereço localhost com a porta mapeada pelo Docker para o container host e verifique que agora o host 1 apresenta a página de início do Apache e o host 2 apresenta a página de início do nginx.

Caso deseje testar, descomente as 3 linhas referente ao uso do `apache2_module` no playbook e comente as 2 linhas logo abaixo, que usam o módulo `command` para executar `a2enmod rewrite`. Ao tentar executar o playbook com o mesmo comando acima ele vai falhar com a mensagem "_ERROR! couldn't resolve module/action 'community.general.apache2_module'_".
Para solucionar, execute o comando descrito no comentário do próprio playbook para instalar o módulo faltante.
